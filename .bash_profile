# linux
alias c='clear'
alias l='ls -lah'
alias b='cd ..'
alias bb='cd ../..'
alias sb='source ~/.bashrc'
alias nb='nano ~/.bashrc'
alias sites='cd /var/www'

# nginx
alias snr='service nginx reload'

# docker
alias dps='docker ps'
alias dpsa='docker ps -a'
alias dsp='docker system prune'
alias dcu='docker-compose up'
alias dcud='docker-compose up -d'
alias dcd='docker-compose down'
alias dvr='docker volume rm'
alias dvl='docker volume ls'

# tmux
alias tl='tmux ls'
alias tn='tmux new -t'
alias ta='tmux a -t'
alias tk='tmux kill-session -t'

# django
alias mi='./mng-api.sh migrate'
alias ma='./mng-api.sh makemigrations'
alias mb='./mng-api.sh bash'
alias ms='./mng-api.sh shell'
alias mau='./mng-api.sh admin_user'
alias mdd='./mng-api.sh drop_db '
alias mpd='./mng-api.sh populate_db '

# git
alias gc='git clean -f'
alias grh='git reset --hard HEAD'
alias grv='git remote -v'
alias gpom='git push origin master'
alias gpum='git pull upstream master'
alias grau='git remote add upstream'